<?php

namespace Sun\Helloworld\Api;

use Magento\Framework\Exception\LocalizedException;
use Sun\Helloworld\Api\Data\InternshipInterface;

interface InternshipRepositoryInterface
{
    /**
     * Retrieve block.
     *
     * @param int $InternshipId
     * @return InternshipInterface
     * @throws LocalizedException
     */
    public function getById($InternshipId);
}
