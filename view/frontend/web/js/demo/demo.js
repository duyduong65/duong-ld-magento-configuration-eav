define([
    'jquery',
    'Sun_Helloworld/js/demo/demo'
], function ($, _) {
    'use strict';
    $.widget('sun.demo',{
        _create: function() {
            $(this.element).owlCarousel(this.options);
        }
    });

    return $.sun.demo;

});
