var config = {
    paths: {
        'owlcarousel': 'Sun_Helloworld/js/owl-carousel/owl.carousel',
        'demo': 'Sun_Helloworld/js/demo/demo',
    },
    shim: {
        owlcarousel: {
            'deps': ['jquery'],
        },
        demo: {
            'deps': ['jquery'],
        }
    },
    map: {
        '*': {
            catalogAddToCart: 'Sun_Helloworld/js/after-catalog-add-to-cart'
        }
    },
};
