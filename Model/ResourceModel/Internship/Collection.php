<?php

namespace Sun\Helloworld\Model\ResourceModel\Internship;

use Sun\Helloworld\Model\Internship;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Internship::class,
            \Sun\Helloworld\Model\ResourceModel\Internship::class
        );
    }
}
