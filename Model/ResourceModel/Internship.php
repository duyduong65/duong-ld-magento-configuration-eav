<?php

namespace Sun\Helloworld\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Internship extends AbstractDb
{
    const TABLE = 'admin_user';
    const PRIMARY_KEY = 'user_id';
    /**
     * Internship construct
     */
    protected function _construct()
    {
        $this->_init(self::TABLE, self::PRIMARY_KEY);
    }
}
