<?php

namespace Sun\Helloworld\Model;

use Magento\Framework\Model\AbstractModel;
use Sun\Helloworld\Api\Data\InternshipInterface;

class Internship extends AbstractModel implements InternshipInterface
{
    /**
     * Config construct
     */
    public function _construct()
    {
        $this->_init(\Sun\Helloworld\Model\ResourceModel\Internship::class);
        parent::_construct();
    }
}
