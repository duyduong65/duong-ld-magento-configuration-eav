<?php

namespace Sun\Helloworld\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Sun\Helloworld\Api\InternshipRepositoryInterface;

class InternshipRepository implements InternshipRepositoryInterface
{
    protected $resource;
    protected $internshipFactory;

    /**
     * InternshipRepository constructor.
     * @param ResourceModel\Internship $resource
     * @param InternshipFactory $blockFactory
     */
    public function __construct(
        \Sun\Helloworld\Model\ResourceModel\Internship $resource,
        InternshipFactory $blockFactory
    ) {
        $this->resource = $resource;
        $this->internshipFactory = $blockFactory;
    }
    /**
     * @inheritDoc
     */
    public function getById($InternshipId)
    {
        $block = $this->internshipFactory->create();
        $this->resource->load($block, $InternshipId);
        if (!$block->getId()) {
            throw new NoSuchEntityException(__('The User with the "%1" ID doesn\'t exist.', $InternshipId));
        }
        return $block;
    }
}
