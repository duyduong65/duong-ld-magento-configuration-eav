<?php

namespace Sun\Helloworld\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CatalogPageLoadAfter implements ObserverInterface
{

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $price = 10;
        $item = $observer->getEvent()->getCollection();
        foreach ($item as $it) {
            $it->setPrice($price);
        }
    }
}
