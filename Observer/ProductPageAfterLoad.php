<?php

namespace Sun\Helloworld\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductPageAfterLoad implements ObserverInterface
{

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $price = 10;
        $item = $observer->getEvent()->getData('product');
        $item->setPrice($price);
    }
}
