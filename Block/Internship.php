<?php

namespace Sun\Helloworld\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Sun\Helloworld\Api\Data\InternshipInterface;
use Sun\Helloworld\Api\InternshipRepositoryInterface;
use Sun\Helloworld\Model\InternshipFactory;

class Internship extends Template
{
    protected $_internshipRepo;
    protected $_internshipFactory;

    /**
     * Internship constructor.
     * @param Template\Context $context
     * @param InternshipRepositoryInterface $internshipRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        InternshipRepositoryInterface $internshipRepository,
        InternshipFactory $internshipFactory,
        array $data = []
    ) {
        $this->_internshipFactory = $internshipFactory;
        $this->_internshipRepo = $internshipRepository;
        parent::__construct($context, $data);
    }

    /**
     * GetById method
     *
     * @param int $id
     *
     * @return Template|InternshipInterface
     * @throws LocalizedException
     */
    public function getById($id)
    {
        return $this->_internshipRepo->getById($id);
    }

    /**
     * Get All user method
     *
     * @return mixed
     */
    public function getAllUser()
    {
        return $this->_internshipFactory->create()->getCollection();
    }
}
