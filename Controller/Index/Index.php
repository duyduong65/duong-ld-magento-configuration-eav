<?php

namespace Sun\Helloworld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Authorization;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Sun\Helloworld\Helper\Data;

class Index extends Action
{
    protected $_pageFactory;
    protected $_helperData;
    protected $_authorization;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Data $data
     * @param Authorization $authorization
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Data $data,
        Authorization $authorization
    ) {
        $this->_helperData = $data;
        $this->_pageFactory = $pageFactory;
        $this->_authorization = $authorization;
        return parent::__construct($context);
    }

    /**
     * IsAllowed method
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Customer::manage');
    }

    /**
     * Action method
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        return $this->_pageFactory->create();
    }
}
