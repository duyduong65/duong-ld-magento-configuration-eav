<?php

namespace Sun\Helloworld\Plugin;

class Product
{
    /**
     * Custom method
     *
     * @param \Magento\Catalog\Model\Product $subject
     * @param int $result
     * @return int
     */
    public function afterGetPrice(\Magento\Catalog\Model\Product $subject, $result)
    {
        return 10;
    }
}
