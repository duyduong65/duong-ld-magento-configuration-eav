<?php

namespace Sun\Helloworld\Plugin;

use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\Manager;
use Sun\Helloworld\Helper\Data;

class Message
{
    protected $_manager;
    protected $_data;

    /**
     * Message constructor.
     *
     * @param ManagerInterface $manager
     * @param Data $data
     */
    public function __construct(
        ManagerInterface $manager,
        Data $data
    ) {
        $this->_manager = $manager;
        $this->_data = $data;
    }

    /**
     * @inheritDoc
     *
     * @param Manager $subject
     * @param string $text
     * @return string
     */
    public function beforeAddSuccessMessage(\Magento\Framework\Message\Manager $subject, $text)
    {
        $value = $this->_data->getGeneralConfig('title');
        $text = $value . $text;
        return $text;
    }
}
